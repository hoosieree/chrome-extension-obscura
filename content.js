'use strict';
const C=chrome, d=document.createElement('div'),
      update=(o={})=>{
          if('tog' in o){d.style.height = (o.tog.newValue | o.tog)+'00%';}
          if('color' in o){d.style.backgroundColor = o.color.newValue || o.color;}
      };

d.style='mix-blend-mode:multiply; z-index:2147483647; position:fixed; width:100%; top:0; right:0; pointer-events:none;';
document.documentElement.insertBefore(d, document.documentElement.firstChild);
C.storage.local.get(null, update);
C.runtime.onMessage.addListener(update);
C.storage.onChanged.addListener(update);
