# obscura
*window tint for the web*
![](images/screenshot.png)

As of version 3.0, obscura is available on the [Chrome Web Store][store] for free.


# features
- toggle on/off
- color picker to control hue and lightness

# install
1. go to the [Chrome Web Store][store]
2. click "add to Chrome"

# Why?
Blue light stimulates our brains and disrupts our sleep; this extension filters out some of that blue light to make browsing in the evening less unhealthy.
Chrome OS and some other operating systems don't expose the ability to control the screen's color temperature natively, but if you can add an extension to Chrome you're in luck.

# Okay, but why *yet another brightness extension*?
I wasn't satisfied with the other options.
Specifically, I wanted something simple enough that I could be certain it was only doing its job (and nothing malicious).

Obscura emphasiszes:

1. Security 
  - only does its job
  - doesn't inspect pages (adds its own translucent overlay instead) except to check if they're PDFs
  - only stores/syncs its own settings
  - source code is **short** which means auditing it is feasible
  - no analytics, no tracking, no ads
  - does its job and nothing else
2. Performance
  - does less work than similar extensions so pages load quick
  - only runs at page load, or when you make adjustments
3. Simplicity 
  - about 60 lines of JavaScript (3214 bytes)
  - no dependencies
  - no network access (uses `window.localStorage` to save your settings)

# DIY install
1. clone or download this repo
2. go to `chrome://extensions`
3. enable developer mode
4. choose `Load unpacked extension...` and click on the folder where you downloaded this repo
5. refresh already-open pages to run the extension, as it only runs at page load time

# Changelog
- `3.3.1` some sites don't respect `hidden` attribute, workaround: shrink div to 0 instead of hiding it
- `3.3.0` remove PDF check (Chrome fixed their bug)
- `3.0.0` change price to $0

[store]: https://chrome.google.com/webstore/detail/obscura/nhlkgnilpmpddehjcegjpofpiiaomnen
