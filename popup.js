'use strict';
let XY=[150,40];
const C=chrome, S=C.storage.local,
      send=x=>C.tabs.query({active:true,currentWindow:true}, t=>C.tabs.sendMessage(t[0].id, x)),
      torgba=x=>`rgba(${x[0]}, ${x[1]}, ${x[2]}, ${x[3]/255})`,
      dropper=(x,y)=>torgba(c.getImageData(x,y,1,1).data),
      target=(x,y)=>{c.fillStyle = 'black'; c.beginPath(); c.arc(x,y,30,0,Math.PI*2,true); c.stroke();},
      touch_xy=t=>{const r=c.canvas.getBoundingClientRect(), f=(x,ox,r)=>Math.floor(Math.min(Math.max(x,ox+1),r-1)-ox);
                   return[f(t.pageX, r.left, r.right), f(t.pageY, r.top, r.bottom)];},
      init_c=()=>{
          c.clearRect(0,0,c.canvas.width,c.canvas.height);
          c.canvas.style.backgroundColor='white';
          const g1=c.createLinearGradient(0, 0, c.canvas.width, 0), // horiz, hue
                g2=c.createLinearGradient(0, 0, 0, c.canvas.height);  // vert, lightness
          g1.addColorStop(0.2,'rgba(255,120,120,0)');
          g1.addColorStop(1,'rgba(255,130,70,0.99)');
          g2.addColorStop(0,'rgba(0,0,0,0)');
          g2.addColorStop(1,'rgba(0,0,0,0.8)');
          c.fillStyle = g1; c.fillRect(0,0,c.canvas.width,c.canvas.height);
          c.fillStyle = g2; c.fillRect(0,0,c.canvas.width,c.canvas.height);},
      xy_send=(xy)=>{init_c(); target(...xy); send({xy, color:dropper(...xy)});},
      xy_save=(xy)=>{init_c(); target(...xy); S.set({xy, color:dropper(...xy)});},
      c=document.getElementById('c').getContext('2d'),
      tog=document.getElementById('tog');

window.addEventListener('load',()=>{
    S.get(null, o=>{
        if('tog' in o){tog.checked = o.tog;} else {S.set({tog:tog.checked});}
        if('xy' in o){init_c(); target(...o.xy); XY=o.xy;} else {S.set({xy:XY});} // color's xy coords in canvas
        if(!('color' in o)){S.set({color:dropper(...XY)});} // hardcoded default xy location
    });
    tog.addEventListener('change', x=>{S.set({tog:tog.checked});});
    c.canvas.addEventListener('mousemove', x=>{if(x.buttons!==1){return;} xy_send([x.offsetX, x.offsetY]);});
    c.canvas.addEventListener('mouseup', x=>xy_save([x.offsetX, x.offsetY]));
    c.canvas.addEventListener('touchmove', x=>xy_send(touch_xy(x.targetTouches[0])));
    c.canvas.addEventListener('touchend', x=>xy_save(touch_xy(x.changedTouches[0])));
});
window.addEventListener('keydown', e=>{
    if(e.key==='x'){tog.checked = !tog.checked; S.set({tog:tog.checked});}
});
